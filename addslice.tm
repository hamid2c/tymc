$ 'no_check_ranges'
function intArray z = addslice(intArray x, intArray y)
	if (rows(x) < 3 || columns(x) < 3 || rows(y) < 3 || columns(y) < 3)
		error('Matrices should be of size at least 3x3')
	end
	createArray(z, 3, 3)
	z = x(1:2, 1:2) + y(2:3, 2:3)	
end

