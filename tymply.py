
_keywords = ["break", "case", "catch", "continue", "else", "elseif", "end", 
             "for", "function", "global", "if", "otherwise", "persistent", 
             "return", "switch", "try", "while", "float", "int", "bool", "real",
             "intArray", "realArray"]

reserved = dict( (x, x.upper()) for x in _keywords )

tokens = [
    'NAME', 'NUMBER', 'STRING',
    'COMMA', 'SEMICOLON', 'NEWLINE',
    'DOTTIMES', 'DOTDIVIDE', 'DOTPOWER', 'DOTRDIVIDE',
    'NOTEQUAL', 'ISEQUAL', 'TRANS', 'CONJTRANS',
    'LESS', 'GREATER', 'LESSEQUAL', 'GREATEREQUAL',
    'AND', 'OR', 'NOT', 'ELOR', 'ELAND',
    'LBRACKET', 'RBRACKET', 'LCURLY', 'RCURLY', 'LPAREN', 'RPAREN',
    'LAMBDA',
    'COMMENT', 'DOLLAR'
    ] + reserved.values()

literals = ['=', '+', '-', '*', '/', '\\', '^', ':', "'", '.']

states = (
    ('comment', 'exclusive'),
    ('inlist',  'inclusive'),
    ('inparen', 'inclusive'),
    )

# def t_comment(t):
#     r'%(.*)'
#     t.type = 'COMMENT'
#     t.value = '%s'%t.value[1:]
#     t.lexer.lineno += 1
#     return t

def t_LPAREN(t):
    r'[({]'
    t.lexer.push_state('inparen')
    return t

def t_inparen_END(t):
    'end'
    t.value = 'end'
    t.type = 'NUMBER'
    return t

def t_inparen_RPAREN(t):
    r'[)}]'
    t.lexer.pop_state()
    return t

def t_LBRACKET(t):
    r'\['
    t.lexer.push_state('inlist')
    return t

def t_inlist_RBRACKET(t):
    r'\]'
    t.lexer.pop_state()
    return t

def t_LCURLY(t):
    r'\{'
    t.lexer.push_state('inlist')
    return t

# cannot do this because [a(1,2) b] = min(1:4);
#def t_inlist_COMMA(t):
#    r','
#    t.type = 'LISTCOMMA'
#    return t

def t_inlist_RCURLY(t):
    r'\}'
    t.lexer.pop_state()
    return t

t_COMMA = ','
t_SEMICOLON = r';'

# Comments
def t_PERCENT(t):
    r'%'
    t.lexer.push_state('comment')

def t_comment_body(t):
    r'([^\n]+)'
    t.type = 'COMMENT'
    t.lexer.pop_state()
    return t

t_comment_ignore = '.*'

def t_comment_error(t):
    pass


# Tokens

t_DOTTIMES = r'\.\*'
t_DOTDIVIDE = r'\./'
t_DOTRDIVIDE = r'\.\\'
t_DOTPOWER = r'\.\^'
t_NOTEQUAL = r'~='
t_ISEQUAL = r'=='
t_LESS = r'<'
t_GREATER = r'>'
t_LESSEQUAL = r'<='
t_GREATEREQUAL = r'>='
t_ELAND = r'&'
t_ELOR = '\|'
t_AND = r'&&'
t_OR = '\|\|'
t_NOT = '~'

t_DOLLAR = r'\$'

def t_NAME(t):
    r'[a-zA-Z][a-zA-Z0-9_]*'
    t.type = 'NAME'
    if t.value in reserved:
        t.type = reserved.get(t.value)    # Check for reserved words
    #else:
    #    t.value = _check_name(t.value)
    return t

t_LAMBDA = r'@'

t_TRANS = r"\.'"
t_CONJTRANS = r"'"

def t_STRING(t):
    r"'((?:''|[^\n'])*)'"
    pos = t.lexer.lexpos - len(t.value)
    if pos == 0:
        return t
    
    prec = t.lexer.lexdata[pos-1]
    if prec == '.':
        t.value = ".'"
        t.type = "TRANS"
        t.lexer.lexpos = pos + 2
    elif prec in ' \t[{(=;,\n':
        # it's a string, translate "''" to 
        t.value = "'%s'"%t.value[1:-1].replace("\\", r"\\")
        t.value = "'%s'"%t.value[1:-1].replace("''", r"\'")
    else:
        t.value = "'"
        t.type = "CONJTRANS"
        t.lexer.lexpos = pos + 1
    return t

def t_NUMBER(t):
    r'(?:\d+\.\d*|\d*\.\d+|\d+)(?:[e|E]-?\d+|)'
    try:
        float(t.value)
    except ValueError:
        _print_error("Is this really a float?", t.value)
    return t

def t_COMMENT(t):
    r'%'
    global _comment
    _comment = t.value
    t.lexer.lineno += 1
    #pass
    # No return value. Token discarded

t_ignore = " \t"

def t_NEWLINE(t):
    r'\n'
    pass

# semicolon has a different function inside of [] and {}
def t_inlist_SEMICOLON(t):
    r';'
    t.type = 'COMMA'
    t.value = 'SEMICOLON'
    return t
    #pass

def t_error(t):
    _print_error("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    
# Build the lexer
import lex
lex.lex()


# Parsing rules

precedence = (    
    ('left', 'TRANS', 'CONJTRANS'),
    ('nonassoc', 'LESS', 'GREATER'),
    ('left', '+', '-'),
    ('left', '*', '/', 'DOTTIMES', 'DOTDIVIDE'),
    ('left', '^', 'DOTPOWER'),
    ('right', 'UMINUS', 'UPLUS'),
    )

from symtab import *
from translatedstat import *
import sys
import builtins as bi
# dictionary of names
names = { }
_key_stack = []
_switch_stack = []
# A flag which indicates whether
# we are going to print the first case in the switch block
first_case = False
# for generating condition variable names in nested loops
for_nested_counter = 0
# for generating slice names
slice_counter = 0
# 
indexer_counter = 0

# A buffer containing text which is supposed to be printed before the current statement, see print_statement
print_buf = ''

# Pointer to current symbol table
env = None
_tabs = 0
_comment = None
TABSHIFT = 4
linenum = 0
string_type = type('')

# program switches/options
opt_init_vars = True
opt_check_ranges = True
opt_one_based_arrays = True

def _reset():
    global _tabs, names, _key_stack, _switch_stack, _comment, TABSHIFT, for_nested_counter, first_case, env
    names = { }
    _key_stack = []
    _switch_stack = []
    _tabs = 0
    linenum = 0
    _comment = None
    TABSHIFT = 4
    first_case = False
    for_nested_counter = 0
    print_buf = ''
    slice_counter = 0
    indexer_counter = 0
    # Initialize env
    env = symtab(None)
    env.table.clear()
    for funcname in bi.funcs.keys():
        env.put(funcname, symbol(funcname, type_function, linenum, env))
    env.put('createArray', symbol('createArray', type_function, linenum, env))
    

_reset()


def _pop_from_key_stack():
    global _key_stack
    if len(_key_stack) < 1:
        _print_error('An "end" without matching keyword!')
        _reset()
        return None
    return _key_stack.pop()

def match_keyword(k):
    if (_key_stack[-1] != k):
        _print_error('Keyword ' + k +' is expected')

def _print_error(s):
    print 'line '+str(linenum) +': ' + s


def p_statement_list(p):
    '''statement_list : statement
                      | statement COMMA
                      | statement SEMICOLON
                      | option_list'''
        
    p[0] = p[1]
    if p[1]:
        print_statement(p[1])


def p_statement_list2(p):
    '''statement_list : statement_list SEMICOLON statement
                      | statement_list COMMA statement
                      | statement_list statement'''

    #p[0] = p[1] + '\n' +  p[-1]
    print_statement(p[-1])

def p_option_list(p):
    '''option_list : option'''
    pass
                   
#    global opt_one_based_arrays
#
#   if p[1] == '$':
#       if p[2].strip() == "'zero_based_arrays'":
#           opt_one_based_arrays = False

def p_option_list2(p):
    '''option_list : option_list option'''
    pass

def p_option(p):
    '''option : DOLLAR STRING'''
    global opt_one_based_arrays, opt_check_ranges, opt_init_vars

    def disable_opt_one_based_arrays():
        global opt_one_based_arrays
        opt_one_based_arrays = False
    def disable_opt_check_ranges():
        global opt_check_ranges
        opt_check_ranges = False
    def disable_opt_init_vars():
        global opt_init_vars
        opt_init_vars = False
    
        
    opt_table = {"'zero_based_arrays'": (disable_opt_one_based_arrays),
                 "'no_check_ranges'": (disable_opt_check_ranges),
                 "'no_init_vars'": (disable_opt_init_vars)}

    key = p[2].strip()
    if opt_table.has_key(key):
        opt_table[key]()
    else:
        _print_error("Unknow option: " + key)

out_writer = sys.stdout
def print_statement(stat):
    global _tabs, TABSHIFT, out_writer, print_buf

    if print_buf != '':
        out_writer.write(print_buf)

    if stat.text == '':
        return
    block_keywords = ["case", "}else", 
             "for", "if", "switch", "while", "DEFUN_DLD"]
    try:
        k = stat.text.split()[0]
    except AttributeError:
        #print stat
        _print_error('oops!')
        return
    
    if opt_init_vars:
        if stat.kind == kind_typedecl:
            stat.text += ' = ' +  stat.other[0].init_value
    
    if  stat.semicolon_at_end:
        stat.text +=  ';'
    dedent = False # do indentation in reverse?
    if (k):
        if k in block_keywords:
            dedent = True

    if dedent:
        assert _tabs>=1
        _tabs -= 1
    
    print>> out_writer, (_tabs*TABSHIFT*' ' + stat.text)
    
    if dedent:
        _tabs += 1
        
    print_buf = ''

def p_statement_expr(p):
    '''statement : expression'''
    p[0] = p[1]
    #print('rule 2')

def p_statement_typedecl(p):
    '''statement : typedecl'''
    p[0] = p[1]
    
def p_statement_while(p):
    '''statement : WHILE expression'''
    global _tabs, _key_stack, env
    env = symtab(env)
    p[0] = translatedstat('while (%s) {'%p[2].text, False)
    _key_stack.append('while')
    #_print(p[0])
    _tabs += 1

def p_statement_break(p):
    """statement : BREAK"""
    p[0] = translatedstat('break', True)

def p_statement_continue(p):
    """statement : CONTINUE"""
    p[0] = translatedstat('continue', True)
    #_print(p[0])

def p_statement_return(p):
    """statement : RETURN"""
    p[0] = translatedstat('return', True)

        
def p_statement_for(p):
    '''statement : FOR NAME "=" expression'''
    #'''statement : FOR NAME "=" slice'''
    
    global _tabs, _key_stack, for_nested_counter, env, print_buf

    if not p[4].kind == kind_sliceexpr:
        typ = env.get(p[2]).type
        indent = _tabs * TABSHIFT * ' '
        if not is_array(typ):
            _print_error(p[2] + ' should be an array')
            sys.exit(1)

        expr_array = '_expr_array' + str(for_nested_counter)
        col_counter = '_col_' + str(for_nested_counter)
        print_buf += indent + ('%s %s = %s;\n' % (type2code[typ], expr_array, p[4]))
        #for_nested_counter += 1
        print_buf += indent + ('for (; %s < %s.columns(); %s++) {\n'%(col_counter, expr_array, col_counter))
        text= '%s = %s.index(idx_vector::colon, idx_vector(%s))' % (p[2], expr_array, col_counter)
        p[0] = translatedstat(text, True)
    else:
        step_val = '1'
        init_val = ''
        exit_val = ''
        slic = p[4].other
        if len(slic)==3:
            init_val = slic[0].text
            step_val = slic[1].text
            exit_val = slic[2].text
        else:
            init_val = slic[0].text
            exit_val = slic[1].text

        step_text = '%s += (%s)'%(p[2], step_val)
        step_num = None
        try:
            step_num = int(step_val)
        except ValueError:
            try: 
                step_num = float(step_val)
            except ValueError:
                pass
        if step_num != None:
            if step_num > 0:     
                p[0] = translatedstat('for (%s = (%s); %s <= (%s); %s) {'%(p[2], init_val, p[2], exit_val, step_text), False)
            else:
                p[0] = translatedstat('for (%s = (%s); %s >= (%s); %s) {'%(p[2], init_val, p[2], exit_val, step_text), False)
        else:
            indent = _tabs*TABSHIFT*' '
            for_cond = '_for_cond_' + str(for_nested_counter)
            text = 'bool %s;\n'%(for_cond)
            text += '%sif (%s > 0) {\n'% (indent, step_val)
            indent2 = indent + TABSHIFT*' '
            text += '%s%s = (%s <= (%s));\n' % (indent2, for_cond, p[2], exit_val)
            text += indent+'} else {\n'
            text += '%s%s = (%s >= (%s));\n%s}\n' % (indent2, for_cond, p[2], exit_val, indent) 
            text += '%sfor (%s = (%s); %s; %s) {'%(indent, p[2], init_val, for_cond, step_text)
            p[0] = translatedstat(text, False)
    _key_stack.append('for')
    for_nested_counter += 1
    #_print(p[0])
    env = symtab(env)
    _tabs += 1
    
def p_expression_slice(p):
    """slice : expression ':' expression ':' expression 
             | expression ':' expression
             | ':' """
    if p[1] == ':':
        p[0] = (translatedstat(':', True, kind_sliceexpr), )
        return
    if len(p) == 6:
        p[0] = (p[1],p[3],p[5])
    else:
        p[0] = (p[1],p[3])

def p_expression_mslice(p):
    "expression : slice"
    p[0] = translatedstat('slice', True, kind_sliceexpr)
    p[0].other = p[1]
    
def p_expression_group(p):
    "expression : LPAREN exprlist RPAREN"
    #p[0] = '(%s)'%p[2]
    p[0] = translatedstat('(%s)'%p[2].text, True, kind_expr)


def p_expression_items(p):
    "expression : name_sub"
    global print_buf
    if p[1].kind == kind_slicelist:
        # p[1].other is a list of expressions (i.e (expr1, expr2, ...))
        # Any of them can be a slice
        temp = gen_expr_parameters(p[1].other)
        array_name = p[1].text
        typ = env.get(array_name).type
        if not is_array(typ):
            _print_error(array_name + ' must be an array')
            sys.exit(1)
        t = type2code[typ]
        p[0] = translatedstat('((%s)%s.index(%s))'%(t, p[1].text, temp), True, kind_expr)
    else:
        vfname = ''
        try:
            vfname = p[1].other
        except AttributeError:
            pass
        #print "here! " + vfname + " -- " + p[1].text
        if vfname != '':
            sym = env.get(vfname)
            if (sym != None):
                if sym.type == type_intArray:
                    p[0] = translatedstat('%s.value()'%p[1].text, True, kind_expr)
                    return
        p[0] = translatedstat('%s'%p[1].text, True, kind_expr)

def strtouple(t):
    ret = ''
    for i in t:
        ret += str(i) + '__'
    return ret;

def p_error(p):
    if p:
        _print_error("Synatx error in " + p.value)
    else:
        _print_error("Syntax error in EOF")

def p_expression_sub(p):
    "name_sub : NAME LPAREN exprlist RPAREN"

    global print_buf, slice_counter 
    
    sym = env.get(p[1])
    if sym == None:
        _print_error(p[1] + " is not declared")
        sys.exit(1)
    else:
        typ = sym.type
        if typ == type_function:
            if p[1] == 'createArray':
                params = p[3].text.split(',')
                arr_name = params[0].strip()
                s = env.get(arr_name)
                temp = ''
                for t in params[1:]:
                    temp += t + ', '
                text = type2code[s.type] + ' ' + arr_name + '(dim_vector(' + temp[:-2] + '))'
                arrname = params[0].strip()
                arrsym = env.get(arrname)
                assert arrsym != None
                arrsym.alloc_line = linenum
                p[0] = translatedstat(text, True, kind_expr)
            elif bi.funcs.has_key(p[1]):
                p[0] = translatedstat(bi.funcs[p[1]](p[3].text), True, kind_expr)
        elif is_array(typ):
            text = ''
            pars = p[3].text
            if opt_one_based_arrays:
                pars = ''
                for expr in p[3].other:
                    if expr.kind == kind_sliceexpr:
                        p[0] = translatedstat(p[1], True, kind_slicelist)
                        p[0].other = p[3].other
                        return
                    pars += expr.text.strip() + '-1' + ', '
                pars = pars[:-2]

            if opt_check_ranges:
                text = '%s.checkelem(%s)'%(p[1], pars)
            else:
                text = '%s.xelem(%s)'%(p[1], pars)
            p[0] = translatedstat(text, True, kind_expr)
            p[0].other = p[1]
        else:
            p[0] =  translatedstat('%s(%s)'%(p[1], p[3].text), True, kind_expr)

def p_expr_list(p):
    '''exprlist : exprlist COMMA expression'''
    p[0] = translatedstat('%s, %s'%(p[1].text, p[3].text), True, kind_expr)
    p[1].other.append(p[3])
    p[0].other = p[1].other

def p_expr_list_2(p):
    'exprlist : expression'
    p[0] = translatedstat(p[1].text, True, kind_expr)
    p[0].other = [p[1]]
    


#def p_expression_name_list(p):
#    '''name_list : name_list COMMA NAME'''
#    p[0] = '%s, %s'%(p[1], p[3])

#def p_expression_name_list_2(p):
#    '''name_list : NAME'''
#    p[0] = p[1]
def is_array(typ):
    return (typ == type_intArray or typ == type_realArray)
def p_typedecl(p):
    '''typedecl : INT NAME
    | BOOL NAME 
    | FLOAT NAME
    | REAL NAME
    | INTARRAY NAME
    | REALARRAY NAME'''
    
    global env
    name = p[2]
    if env.get(name) != None:
        _print_error(name + " is already declared")
        sys.exit(1)
    else:
        typ = None
        try:
            typ = str2type[p[1]]
        except KeyError:
            _print_error('Undefined type: '+p[1])
            sys.exit(1)
            
        s = type2code[typ]

        text = ''
        if not is_array(typ):
            text = type2code[typ] + ' ' + name
        p[0] = translatedstat(text, not is_array(typ), kind_typedecl)
        p[0].kind = kind_typedecl
        p[0].other = (typ, name)
        p[0].type = typ
        sym = symbol(name, typ, linenum, env)
        if is_array(typ):
            sym.alloc_line = -1
        else:
            sym.alloc_line = linenum
    
        env.put(name, sym)

def p_typedecl_list(p):
    '''typedecl_list : typedecl_list COMMA typedecl'''
    p[0] = translatedstat('%s, %s'%(p[1].text, p[3].text), False)
    p[1].other.append(p[3].other)
    p[0].other = p[1].other

def p_typedecl_list2(p):
    '''typedecl_list : typedecl'''
    #typedecl_list.other would be a list of pairs, each pair
    #is a declaration that consists of a type and a name
    p[0] = translatedstat(p[1].text, False)
    p[0].other = [p[1].other]

outsymbols = []
def p_statement_function(p):
    '''statement : FUNCTION LBRACKET typedecl_list RBRACKET "=" NAME LPAREN typedecl_list RPAREN
                 | FUNCTION LBRACKET typedecl_list RBRACKET "=" NAME
                 | FUNCTION typedecl_list "=" NAME LPAREN typedecl_list RPAREN
                 | FUNCTION typedecl_list "=" NAME
                 | FUNCTION NAME LPAREN typedecl_list RPAREN
                 | FUNCTION NAME'''
    global env, _tabs
    argoutlist = None
    funcname = None
    arginlist = None
    env = symtab(env)
    if '=' in p: # there are output arguments
        if p[2] == '[':
            argoutlist, funcname = p[3], p[6]
            if '(' in p: # there are input arguments
                arginlist = p[8]
        else:
            argoutlist, funcname = p[2], p[4]
            if '(' in p:
                arginlist = p[6]
    else:
        funcname = p[2]
        if '(' in p:
            arginlist = p[4]
    p[0] = translatedstat(genfunction(funcname,arginlist, argoutlist), False)
    _key_stack.append('function')
    env.put(funcname, symbol(funcname, type_function, linenum, env))

def genfunction(funcname, arginlist, argoutlist):
    global TABSHIFT, _tabs, outsymbols, env, opt_check_ranges
    assert env != None
    _tabs = 0 # we don't have indented functions
    docstring = ''  
    text = 'DEFUN_DLD (%s, args, nargout, "%s") {'%(funcname, docstring)
    _tabs = 1
    indent = TABSHIFT*' '
    text += indent + '\noctave_value_list retval;\n'
    if opt_check_ranges and arginlist != None:
        text += indent + 'if ((%s) != %i) { %s }\n' % ('args.length()', len(arginlist.other), r'std::cout<<"invalid input params\n";return retval;')
    if arginlist != None:
        argnum = 0
        for decl in arginlist.other:
            sym = env.prev.table.pop(decl[1])
            sym.table = env
            #sym = symbol(decl[1], decl[0], linenum, env)
            env.put(decl[1], sym)
            text += '\n%s%s %s=%s.%s;'%(indent,type2code[decl[0]], decl[1], 'args('+str(argnum)+')',type2gettermethod[decl[0]])
            argnum += 1

    if opt_check_ranges and arginlist != None:
        text += '\n' + indent + r'if (error_state) { std::cout<<"invalid type of input parameters\n";return retval;}'
            
    outsymbols = []    
    if argoutlist != None:
        for decl in argoutlist.other:
            sym = env.prev.table.pop(decl[1])
            sym.table = env
            #sym = symbol(decl[1], decl[0], linenum, env)
            env.put(decl[1], sym)
            outsymbols.append(sym)
            if not is_array(decl[0]):
                text += '\n%s%s %s'%(indent, type2code[decl[0]], decl[1])
                if (opt_init_vars):
                    text += ' = ' + decl[0].init_value
                text += ';'

    return text

def p_statement_if(p):
    '''statement : IF expression'''
    global _key_stack, _tabs, env
    p[0] = translatedstat('if (%s) {'%p[2].text, False)
    #print('rule IF')
    _key_stack.append('if')
    env = symtab(env)
    _tabs += 1

def p_statement_elseif(p):
    '''statement : ELSEIF expression'''
    global _tabs, _key_stack, env
    # FIXME if p is cellarray we should copare with in
    p[0] = translatedstat('}else if (%s) {'%p[2].text, False)
    match_keyword('if')
    env = symtab(env);
    #_tabs -= TABSHIFT
    #_print(p[0])
    #_tabs += TABSHIFT

def p_statement_else(p):
    '''statement : ELSE'''
    global _tabs, _key_stack, env
    # FIXME if p is cellarray we should copare with in
    p[0] = translatedstat('}else {', False)
    match_keyword('if')
    env = symtab(env)
    #_tabs -= TABSHIFT
    #_print(p[0])
    #_tabs += TABSHIFT

def p_statement_switch(p):
    '''statement : SWITCH expression'''
    global _tabs, _key_stack, _switch_stack, first_case, env
    #svar = '__switch_%d__'%len(_switch_stack)
    #p[0] = '%s = %s\nif 0:\n%spass'%(svar, p[2], ' '*TABSHIFT)
    p[0] = translatedstat('switch (%s) {' % p[2].text, False)
    _key_stack.append('switch')
    first_case = True
    #_switch_stack.append( svar )
    _tabs += 1
    #_print(p[0])
    env = symtab(env)
    #print 'rule SWITCH'

def p_statement_case(p):
    '''statement : CASE expression'''
    global _tabs, _key_stack, _switch_stack, first_case
    # FIXME if p is cellarray we should copare with in
    #p[0] = 'elif %s == %s:'%(_switch_stack[-1], p[2])
    if not first_case:
        indent = _tabs * TABSHIFT * ' '
        p[0] = translatedstat('break;\n%scase (%s):'%(indent, p[2].text), False)
    else:
        p[0] = translatedstat('case (%s):'%p[2].text, False)
        first_case = False
    match_keyword('switch')
    #_tabs -= TABSHIFT
    #_print(p[0])
    #_tabs += TABSHIFT


def p_statement_end(p):
    'statement : END'
    global _tabs, _key_stack, _switch_stack, first_case, for_nested_counter, env, outsymbols
    #print('rule END')
    text = ''
    kw = _pop_from_key_stack()
    if (first_case):
        _print_error('switch block is empty')
        first_case = False
    #if kw == 'switch':
     #   _switch_stack.pop()
    if kw == 'for':
        for_nested_counter -= 1
    if kw == 'function':
        if (len(outsymbols) != 0):
            argnum = 0
            indent = TABSHIFT * ' '
            for sym in outsymbols:
                if sym.alloc_line == -1:
                    _print_error('Variable ' + sym.name + ' which is declared in line ' + str(sym.line) + 'is not allocated')
                text += indent + 'retval('+str(argnum)+') = ' + sym.name + ';\n'
                argnum += 1
            text += indent + 'return retval;\n'
    env = env.prev
    _tabs -= 1
    text += '}'
    p[0] = translatedstat(text, False)

def gen_expr_parameters(exprlist):
    global print_buf, indexer_counter, _tabs, TABSHIFT
    if len(exprlist) == 0 or exprlist == None:
        return ''

    is2 = len(exprlist) <= 2
    indexer_name = ''
    ret = ''
    indent = _tabs * TABSHIFT * ' '
    ind = 0
    for expr in exprlist:
        if expr.kind == kind_sliceexpr:
            if (not is2) and indexer_name == '':
                indexer_name = 'indexer_' + str(indexer_counter)
                indexer_counter += 1                    
                print_buf += indent + 'Array<idx_vector> ' + indexer_name + '(' + str(len(exprlist)) + ');\n'
            start = '0'
            end = '0'
            step = '1'
            is_colon = False
            if len(expr.other) == 1:
                #colon
                is_colon = True
            elif len(expr.other) == 2:
                start = expr.other[0].text
                end = expr.other[1].text
            else:
                start = expr.other[0].text
                step = expr.other[1].text
                end = expr.other[2].text
            if opt_one_based_arrays:
                start += '-1'
                end += '-1'
            end += '+1'
                
            if is2:
                if not is_colon:
                    ret += 'idx_vector(%s, %s, %s)'%(start, end, step)
                else:
                    ret += 'idx_vector::colon'
                ret += ', '
            else:
                # ret remains '' in this case
                if not is_colon:
                    print_buf += '%s.xelem(%i) = idx_vector(%s, %s, %s);\n' % (indexer_name, ind, start, end, step)
                else:
                    print_buf += '%s.xelem(%i) = idx_vector::colon;\n' % (indexer_name, ind)
                ind += 1
        else:
            ret += expr.text + ', '
            
    if ret == '':
        assert indexer_name != ''
        ret = indexer_name
    else:
        ret = ret[:-2]
    return ret
           
    
def p_statement_assign(p):
    '''statement : NAME "=" expression
                 | name_sub "=" expression'''
    #
    is_terminal = type(p[1])==string_type
    if is_terminal:
        if env.get(p[1]) == None:
            _print_error('Undeclared variable: ' + p[1])
            sys.exit(1)
    else: # The second production
        if p[1].kind == kind_slicelist:
            params = gen_expr_parameters(p[1].other)
            p[0] = translatedstat('%s.assign(%s, %s)'%(p[1].text, params, p[3].text))
            return
        
    if is_terminal:
        names[p[1].strip()] = p[3]        
    p[0] = translatedstat('%s = %s'%(p[1], p[3].text), True)

def p_statement_assign2(p):
    '''statement : typedecl "=" expression'''
    p[0] = translatedstat('%s = %s'%(p[1].text, p[3].text), True)

def p_expression_binop(p):
    """expression : expression '+' expression
                  | expression '-' expression
                  | expression '*' expression
                  | expression '/' expression
                  | expression '\\\\' expression
                  | expression '^' expression
                  | expression DOTTIMES expression
                  | expression DOTDIVIDE expression
                  | expression DOTRDIVIDE expression
                  | expression DOTPOWER expression
                  | expression NOTEQUAL expression
                  | expression ISEQUAL expression
                  | expression LESS expression
                  | expression GREATER expression
                  | expression LESSEQUAL expression
                  | expression GREATEREQUAL expression
                  | expression ELAND expression
                  | expression ELOR expression
                  | expression AND expression
                  | expression OR expression"""

    if p[2] == '+'  : p[0] = translatedstat('%s + %s'%(p[1].text, p[3].text), True, kind_binexpr)
    elif p[2] == '-'  : p[0] = translatedstat('%s - %s'%(p[1].text, p[3].text), True, kind_binexpr)
    elif p[2] == '*'  : p[0] = translatedstat('%s * %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '/'  : p[0] = translatedstat('%s / %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '\\' : p[0] = translatedstat('%s /ldiv/ %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '^'  : p[0] = translatedstat('%s ** %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '.*' : p[0] = translatedstat('%s *elmul* %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == './' : p[0] = translatedstat('%s /eldiv/ %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '.\\': p[0] = translatedstat('%s /elldiv/ %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '.^' : p[0] = translatedstat('%s **elpow** %s'%(p[1], p[3]), True, kind_binexpr)
    # conditional and logical
    elif p[2] == '~=' : p[0] = translatedstat('%s != %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '==' : p[0] = translatedstat('%s == %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '<'  : p[0] = translatedstat('%s < %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '>'  : p[0] = translatedstat('%s > %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '<=' : p[0] = translatedstat('%s <= %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '>=' : p[0] = translatedstat('%s >= %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '&'  : p[0] = translatedstat('%s & %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '|'  : p[0] = translatedstat('%s | %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '&&' : p[0] = translatedstat('%s && %s'%(p[1], p[3]), True, kind_binexpr)
    elif p[2] == '||' : p[0] = translatedstat('%s || %s'%(p[1], p[3]), True, kind_binexpr)

    #print('rule BIN_EXP')
    
def p_expression_not(p):
    "expression : NOT expression"
    p[0] = translatedstat('!(%s)'%p[2].text, True, kind_uniexpr)

def p_expression_uminus(p):
    "expression : '-' expression %prec UMINUS"
    p[0] = translatedstat('-%s'%p[2].text, True, kind_uniexpr)

def p_expression_uplus(p):
    "expression : '+' expression %prec UPLUS"
    p[0] = p[2]

def p_expression_string(p):
    "expression : STRING"
    text = '"'+p[1][1:-1]+'"'
    p[0] = translatedstat(text, True, kind_expr)

def p_expression_number(p):
    "expression : NUMBER"
    p[0] = translatedstat(p[1], True, kind_expr)
    #print('rule TERMINAL_NUMBER')

def p_expression_name(p):
    "expression : NAME"
    if env.get(p[1]) == None:
        _print_error('Undeclared variable: ' + p[1])
        sys.exit(1)
    p[0] = translatedstat(p[1], True, kind_expr)
    #print('rule TERMINAL_NAME')


def tym_prep(x):
    """OMPC preprocessor.
    Takes a single line of m-code and returns a tuple of
    stripped m-code and a comment.
    Continuation is requested by the 1st returned value set to None.
    """
    #global _cont, _pinlist
    from re import sub, findall, finditer
    # skip empty statements and take care of possible funny endlines 
    # only '\n' is allowed into the parser
    x = x.replace('\r', '')
    if not x.strip():
        return '', ''
    
    # remove comments
    #x2 = sub(r"'((?:''|[^\n'])*)'", _mysub, x)
    pos = list(finditer(r'\s*%.*', x))
    com = ''
    if pos:
        pos = pos[0].start()
        com = x[pos:].replace('%', r'//', 1)
        x = x[:pos]
        if not x.strip():
            com = com.lstrip()
   
    return x, com
import yacc
yacc.yacc()

def doparse(s):
    s1, com = tym_prep(s)
    if s1:
        yacc.parse(s1)
