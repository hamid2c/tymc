#!/usr/bin/python
import tymply
import sys, os

if __name__=='__main__':
    fname=None
    outf=None
    if len(sys.argv)<2:
        print('usage: tymc input.tm [-out=file|-out=stdout]')
        sys.exit(1)
    else:
        outname = ''
        for opt in sys.argv[1:]:
            if opt[:5] == '-out=':
                outname = opt[5:]
            elif opt == '-no_check_ranges':
                tymply.opt_check_ranges = False
            elif opt == '-no_init_vars':
                tymply.opt_init_vars = False
            elif opt == '-zero_based_arrays':
                tymply.opt_one_based_arrays = False
            else:
                inname=opt
        
        if outname == 'stdout':
            outf = sys.stdout
        elif outname != '':
            outf=open(outname, 'w')
        else:
            outf=open(inname[:-3] + '.cpp', 'w')

    f=open(inname, 'r')        
    tymply.out_writer=outf
    tymply.linenum = 1
    tymply.out_writer.write('#include <octave/oct.h>\n')
    tymply.out_writer.write('#include <iostream>\n')
    tymply.out_writer.write('#include <cstdlib>\n')
    while 1:
        line=f.readline()
        if not line:
            break
        tymply.linenum += 1        
        if line.strip()=='':
            continue
        tymply.doparse(line)

    f.close()
    if outf != sys.stdout:
        outf.close()
