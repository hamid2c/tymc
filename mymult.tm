$ 'zero_based_arrays'
$ 'no_init_vars'
$ 'no_check_ranges'

function intArray z = mymult(intArray x, intArray y)
	int d1x = rows(x)
	int d2x = columns(x)
	int d1y = rows(y)
	int d2y = columns(y)

	if (d2x ~= d1y)
		error('incompatible dimensions')
	end

	createArray(z, d1x, d2y)
	
	int i
	int j
	int k
	for i=0:d1x-1
		for j=0:d2y-1
			z(i, j) = 0
			for k=0:d1y-1
				z(i, j) = z(i, j) + x(i, k)*y(k, j)
			end
		end
	end

end
