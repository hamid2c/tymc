
class cmtype:
    #init_value = ''
    def __repr__(self):
        global str2type
        return type2str[self]
    def __init__(self, dval):
        self.init_value = dval

type_int = cmtype('0')
type_float = cmtype('0.0')
type_bool = cmtype('false')
type_real = cmtype('0.0')
type_intArray = cmtype('')
type_realArray = cmtype('')

type_function = cmtype('')

type_slice = cmtype('')

str2type = {'int': type_int, 'float': type_float, 'bool':type_bool, 'function': type_function, 'intArray': type_intArray, 'realArray': type_realArray, 'real':type_real}

type2str = {type_int: 'int', type_float: 'float', type_bool: 'bool', type_function: 'function', type_intArray: 'intArray', type_realArray: 'realArray', type_real:'realArray'}

type2gettermethod = {type_float: 'array_value()(0)', type_int:'int32_array_value()(0)', type_intArray: 'int32_array_value()', type_bool: 'bool_array_value()(0)', type_real:'array_value()(0)', type_realArray:'array_value()'}

type2code = {type_int: 'int', type_float: 'float', type_bool: 'bool', type_function: 'function', type_intArray: 'int32NDArray', type_realArray:'NDArray', type_real:'double'}

class symbol:
    #type = None
    #line = 0 # declaration line
    #alloc_line = 0 # 0: unknown, -1: unallocated
    #name = ''
    #table = None
    
    def __init__(self, name, type, line, symboltab):
        self.name = name
        self.type = type
        self.line = line
        self.line = 0  # unknown
        self.alloc_line = -1
        self.table = symboltab
    def __repr__(self):
        return self.name

class symtab:
    #table = {}
    #prev = None
    
    def __init__(self, prev_tab):
        self.prev = prev_tab
        self.table = {}

    def put(self, name, sym):
        self.table[name] = sym

    def get(self, name):
        tab = self
        sym = None
        while True:
            try:
                sym = tab.table[name]
                break
            except KeyError:
                if tab.prev != None:
                    tab = tab.prev
                else:
                    return None

        
        assert (sym != None)
        return sym
