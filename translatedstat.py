kind_unknown = 0x00 
kind_binexpr = 0x01 
kind_uniexpr = 0x02
kind_sliceexpr = 0x04
kind_expr = kind_binexpr | kind_uniexpr | kind_sliceexpr #0x07
kind_typedecl = 0x0A #10
kind_slicelist = 0x0B #11xs
#Translated Statement Class, despite its name it can represent any programming construct
#in AST

class translatedstat:

    def __init__(self, text='', semicolon_at_end=False, kind=kind_unknown):
        self.text = text
        self.semicolon_at_end = semicolon_at_end
        self.kind = kind
        #self.type = symtab.type_unknown

    def __repr__(self):
        return self.text

    def __getslice__(self, i, j):
        return self.text[i:j]

    def __getitem__(self, key):
        return self.text[key]

    def split(self):
        return self.text.split()
