function z = mmymult (x, y)
	d1x = rows(x);
	d2x = columns(x);
	d1y = rows(y);
	d2y = columns(y);

	if (d2x ~= d1y)
		error('incompatible dimensions');
	end

	for i=1:d1x
		for j=1:d2y
			z(i, j) = 0;
			for k=1:d1y
				z(i, j) = z(i, j) + x(i, k)*y(k, j);
			end
		end
	end

end
