$ 'no_init_vars'
$ 'no_check_ranges'

function intArray z = mymult1(intArray x, intArray y)
	int d1x = rows(x)
	int d2x = columns(x)
	int d1y = rows(y)
	int d2y = columns(y)

	if (d2x ~= d1y)
		error('incompatible dimensions')
	end

	createArray(z, d1x, d2y)
	
	int i
	int j
	int k
	for i=1:d1x
		for j=1:d2y
			z(i, j) = 0
			for k=1:d1y
				z(i, j) = z(i, j) + x(i, k)*y(k, j)
			end
		end
	end

end
